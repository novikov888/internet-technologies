import express from 'express';
import { resolve } from 'path';
import { __dirname } from './globals.js';
import { readData, writeData } from './fileUtils.js';

const app = express();

const hostname = 'localhost';
const port = 4321;

const shelfs = [];

// Middleware для формирования ответа в формате JSON
app.use(express.json());

// Middleware для логирования запросов
app.use((request, response, next) => {
  console.log(
    (new Date()).toISOString(),
    request.ip,
    request.method,
    request.originalUrl
  );

  next();
});

// Middleware для раздачи статики
app.use('/', express.static(
  resolve(__dirname, '..', 'public')
));

//---------------------------------------------------
// Роуты приложения

// Получение весех стиллажей 
app.get('/shelfs', (request, response) => {
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json(shelfs);
});

// Создание нового стиллажа
app.post('/shelfs', async (request, response) => {
  console.log(request);
  const { shelfName } = request.body;
  shelfs.push({
    shelfName,
    products: []
  });
  await writeData(shelfs);

  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Shelf'${shelfName}' was successfully created`
    });
});

// Создание нового товара
app.post('/shelfs/:shelfId/products', async (request, response) => {
  const { productName, productPrice } = request.body;
  const shelfId = Number(request.params.shelfId);

  if (shelfId < 0 || shelfId >= shelfs.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${shelfId}`
      });
    return;
  }

  shelfs[shelfId].products.push({productName, productPrice});
  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Product '${productName}' was successfully added in shelf '${shelfs[shelfId].shelfName}'`
    });
});

// Изменение товара
app.put('/shelfs/:shelfId/products/:productId', async (request, response) => {
  const productName = request.body.newProductName ;
  const productPrice = request.body.newProductPrice ;
  const shelfId = Number(request.params.shelfId);
  const productId = Number(request.params.productId);

  if (shelfId < 0 || shelfId >= shelfs.length
    || productId < 0 || productId >= shelfs[shelfId].products.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${
          shelfId} or product with id = ${productId}`
      });
    return;
  }

  shelfs[shelfId].products[productId] = { productName, productPrice };
  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `{Product №${productId} was successfully edited in shelf '${shelfs[shelfId].shelfName}'`
    });
});

// Удаление товара
app.delete('/shelfs/:shelfId/products/:productId', async (request, response) => {
  const shelfId = Number(request.params.shelfId);
  const productId = Number(request.params.productId);

  if (shelfId < 0 || shelfId >= shelfs.length
    || productId < 0 || productId >= shelfs[shelfId].products.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${shelfId} or product with id = ${productId}`
      });
    return;
  }

  const deletedProductName = shelfs[shelfId].products[productId];
  shelfs[shelfId].products.splice(productId, 1);
  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Product '${deletedProductName}' was successfully deleted from shelf '${shelfs[shelfId].shelfName}'`
    });
});

// Перенос товара из одного стиллажа в другой
app.patch('/shelfs/:shelfId', async (request, response) => {
  const fromShelfId = Number(request.params.shelfId);
  const { toShelfId, productId } = request.body;

  if (fromShelfId < 0 || fromShelfId >= shelfs.length
    || productId < 0 || productId >= shelfs[fromShelfId].products.length
    || toShelfId < 0 || fromShelfId >= shelfs.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${
          fromShelfId} of ${toShelfId} or product with id = ${productId}`
      });
    return;
  }

  const movedProductName = shelfs[fromShelfId].products[productId];

  shelfs[fromShelfId].products.splice(productId, 1);
  shelfs[toShelfId].products.push(movedProductName);

  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Product '${movedProductName}' was successfully moved from shelf '${shelfs[fromShelfId].shelfName}' to shelf '${
        shelfs[toShelfId].shelfName
      }'`
    });
}); 

//---------------------------------------------------

// Запуск сервера
app.listen(port, hostname, async (err) => {
  if (err) {
    console.error('Error: ', err);
    return;
  }

  console.log(`Out server started at http://${hostname}:${port}`);

  const shelfsFromFile = await readData();
  shelfsFromFile.forEach(({ shelfName, products }) => {
    shelfs.push({
      shelfName,
      products: [...products]
    });
  });
});
