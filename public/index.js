document.addEventListener('DOMContentLoaded', () => {
  const app = new App();
  app.init();
});

class AppModel {
  static async getShelfs() {
    const shelfsRes = await fetch('http://localhost:4321/shelfs');
    return await shelfsRes.json();
  }

  static async addShelf(shelfName) {
    console.log(JSON.stringify({ shelfName }));
    const result = await fetch(
      'http://localhost:4321/shelfs',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ shelfName })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async addProduct({
    shelfId,
    productName,
    productPrice
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${shelfId}/products`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName, productPrice })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async editProduct({
    shelfId,
    productId,
    newProductName,
    newProductPrice
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${shelfId}/products/${productId}`,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ newProductName, newProductPrice })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async deleteProduct({
    shelfId,
    productId
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${shelfId}/products/${productId}`,
      {
        method: 'DELETE'
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async moveProduct({
    fromShelfId,
    toShelfId,
    productId
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${fromShelfId}`,
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ toShelfId, productId })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }
}

class App {
  constructor() {
    this.shelfs = [];
  }

  onEscapeKeydown = ({ key }) => {
    if (key === 'Escape') {
      const input = document.getElementById('add-shelf-input');
      input.style.display = 'none';
      input.value = '';

      document.getElementById('hw-shelf-add-shelf')
        .style.display = 'inherit';
    }
  };

  onInputKeydown = async ({ key, target }) => {
    if (key === 'Enter') {
      if (target.value) {
        await AppModel.addShelf(target.value);

        this.shelfs.push(
          new Shelf({
            tlName: target.value,
            tlID: `TL${this.shelfs.length}`,
            moveProduct: this.moveProduct
          })
        );

        this.shelfs[this.shelfs.length - 1].render();
      }
      
      target.style.display = 'none';
      target.value = '';

      document.getElementById('hw-shelf-add-shelf')
        .style.display = 'inherit';
    }
  };

  moveProduct = async ({ productID, direction }) => {
    let [
      tlIndex,
      productIndex
    ] = productID.split('-T');
    tlIndex = Number(tlIndex.split('TL')[1]);
    productIndex = Number(productIndex);
    const product = { productName: this.shelfs[tlIndex].products[productIndex].productName,
      productPrice: this.shelfs[tlIndex].products[productIndex].productPrice
    };
    const targetTlIndex = direction === 'left'
      ? tlIndex - 1
      : tlIndex + 1;

    try {
      await AppModel.moveProduct({
        fromShelfId: tlIndex,
        toShelfId: targetTlIndex,
        productId: productIndex
      });

      this.shelfs[tlIndex].deleteProduct(productIndex);
      this.shelfs[targetTlIndex].addProduct(product.productName,product.productPrice);
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  async init() {
    const shelfs = await AppModel.getShelfs();
    shelfs.forEach(({ shelfName, products }) => {
      const newShelf = new Shelf({
        tlName: shelfName,
        tlID: `TL${this.shelfs.length}`,
        moveProduct: this.moveProduct
      });
      products.forEach(product => newShelf.products.push(product));
      
      this.shelfs.push(newShelf);
      newShelf.render();
      newShelf.rerenderProducts();
    });

    document.getElementById('hw-shelf-add-shelf')
      .addEventListener(
        'click',
        (event) => {
          event.target.style.display = 'none';

          const input = document.getElementById('add-shelf-input');
          input.style.display = 'inherit';
          input.focus();
        }
      );

    document.addEventListener('keydown', this.onEscapeKeydown);

    document.getElementById('add-shelf-input')
      .addEventListener('keydown', this.onInputKeydown);

    document.querySelector('.toggle-switch input')
      .addEventListener(
        'change',
        ({ target: { checked } }) => {
          checked
            ? document.body.classList.add('dark-theme')
            : document.body.classList.remove('dark-theme');
        }
      );
  }
}

class Shelf {
  constructor({
    tlName,
    tlID,
    moveProduct
  }) {
    this.tlName = tlName;
    this.tlID = tlID;
    this.products = [];
    this.moveProduct = moveProduct;
  }

  onAddProductButtonClick = async () => {
    const newProductName = prompt('🛒 Введите название товара:');
    const newProductPrice = prompt('💲 Введите цену товара:');

    if (!newProductName && !newProductPrice) return;

    const shelfId = Number(this.tlID.split('TL')[1]);
    try {
      await AppModel.addProduct({
        shelfId,
        productName: newProductName,
        productPrice: newProductPrice
      });
      this.addProduct(newProductName, newProductPrice);
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  addProduct = (productName, productPrice) => {
    const product = {
      productName: productName,
      productPrice: productPrice,
    };

    console.log(product);

    document.querySelector(`#${this.tlID} ul`)
      .appendChild(
        this.renderProduct(
          `${this.tlID}-T${this.products.length}`,
          product
        )
      );

    this.products.push({productName, productPrice});
  };

  onEditProduct = async (productID) => {
    const productIndex = Number(productID.split('-T')[1]);
    const oldProduct = this.products[productIndex];

    const newProductName = prompt('🛒 Введите новое название товара', oldProduct.productName);
    const newProductPrice= prompt('💲 Введите новую цену товара', oldProduct.productPrice);

    if ((!newProductName || newProductName === oldProduct.productName) 
          && (!newProductPrice || newProductPrice === oldProduct.productPrice)){
      return;
    }

    const shelfId = Number(this.tlID.split('TL')[1]);
    try {
      await AppModel.editProduct({
        shelfId,
        productId: productIndex,
        newProductName,
        newProductPrice
      });

      this.products[productIndex].productName = newProductName;
      this.products[productIndex].productPrice = newProductPrice;
      document.querySelector(`#${productID} span`)
        .innerHTML = `🛒 ${newProductName} <br> 💲 ${newProductPrice}`;
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  onDeleteProductButtonClick = async (productID) => {
    const productIndex = Number(productID.split('-T')[1]);
    const productName = this.products[productIndex].productName;

    if (!confirm(`🛒 Товар '${productName}' будет удалён. Продолжить?`)) return;

    const shelfId = Number(this.tlID.split('TL')[1]);
    try {
      await AppModel.deleteProduct({
        shelfId,
        productId: productIndex
      });

      this.deleteProduct(productIndex);
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  deleteProduct = (productIndex) => {
    this.products.splice(productIndex, 1);
    this.rerenderProducts();
  };

  rerenderProducts = () => {
    const shelf = document.querySelector(`#${this.tlID} ul`);
    shelf.innerHTML = '';

    this.products.forEach((product, productIndex) => {
      shelf.appendChild(
        this.renderProduct(
          `${this.tlID}-T${productIndex}`,
          product
        )
      );
    });
  };

  renderProduct = ( productID, singleProduct) => {
    const product = document.createElement('li');
    product.classList.add('hw-shelf-product');
    product.id = productID;

    const span = document.createElement('span');
    span.classList.add('hw-shelf-product-text');
    span.innerHTML = `🛒 ${singleProduct.productName} <br>  💲 ${singleProduct.productPrice}`;
    product.appendChild(span);

    const controls = document.createElement('div');
    controls.classList.add('hw-shelf-product-controls');

    const upperRow = document.createElement('div');
    upperRow.classList.add('hw-shelf-product-controls-row');

    const leftArrow = document.createElement('button');
    leftArrow.type = 'button';
    leftArrow.classList.add(
      'hw-shelf-product-controls-button',
      'left-arrow'
    );
    leftArrow.addEventListener(
      'click',
      () => this.moveProduct({ productID, direction: 'left' })
    );
    upperRow.appendChild(leftArrow);

    const rightArrow = document.createElement('button');
    rightArrow.type = 'button';
    rightArrow.classList.add(
      'hw-shelf-product-controls-button',
      'right-arrow'
    );
    rightArrow.addEventListener(
      'click',
      () => this.moveProduct({ productID, direction: 'right' })
    );
    upperRow.appendChild(rightArrow);

    controls.appendChild(upperRow);

    const lowerRow = document.createElement('div');
    lowerRow.classList.add('hw-shelf-product-controls-row');

    const editButton = document.createElement('button');
    editButton.type = 'button';
    editButton.classList.add(
      'hw-shelf-product-controls-button',
      'edit-icon'
    );
    editButton.addEventListener('click', () => this.onEditProduct(productID));
    lowerRow.appendChild(editButton);

    const deleteButton = document.createElement('button');
    deleteButton.type = 'button';
    deleteButton.classList.add(
      'hw-shelf-product-controls-button',
      'delete-icon'
    );
    deleteButton.addEventListener('click', () => this.onDeleteProductButtonClick(productID));
    lowerRow.appendChild(deleteButton);

    controls.appendChild(lowerRow);

    product.appendChild(controls);

    return product;
  };

  render() {
    const shelf = document.createElement('div');
    shelf.classList.add('hw-shelf');
    shelf.id = this.tlID;

    const header = document.createElement('header');
    header.classList.add('hw-shelf-header');
    header.innerHTML = this.tlName;
    shelf.appendChild(header);

    const list = document.createElement('ul');
    list.classList.add('hw-shelf-products');
    shelf.appendChild(list);

    const footer = document.createElement('footer');
    const button = document.createElement('button');
    button.type = 'button';
    button.classList.add('hw-shelf-add-product');
    button.innerHTML = '🛒 Положить товар';
    button.addEventListener('click', this.onAddProductButtonClick);
    footer.appendChild(button);
    shelf.appendChild(footer);

    const container = document.querySelector('main');
    container.insertBefore(shelf, container.lastElementChild);
  }
}
